package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.domain.dto.DetailsMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieDetailsMapper implements Converter<DetailsMovieDto, Movie> {

    @Override
    public DetailsMovieDto convert(Movie from) {
        return new DetailsMovieDto.Builder()
                .image(from.getImage())
                .title(from.getTitle())
                .videoId(from.getVideoId())
                .year(from.getYear())
                .build();
    }
}
